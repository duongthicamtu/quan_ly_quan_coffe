-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 26 Avril 2019 à 05:08
-- Version du serveur :  10.1.10-MariaDB
-- Version de PHP :  5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bt`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `MaAD` int(11) NOT NULL,
  `TenDN` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Pass` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`MaAD`, `TenDN`, `Pass`) VALUES
(1, 'SuperAdmin', 'SuperAdmin');

-- --------------------------------------------------------

--
-- Structure de la table `banngcap`
--

CREATE TABLE `banngcap` (
  `id` int(11) NOT NULL,
  `LoaiBC` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NgayCap` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NgayHH` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SCMND` int(11) NOT NULL,
  `TenBC` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MaBC` int(11) NOT NULL,
  `NoiCap` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `banngcap`
--

INSERT INTO `banngcap` (`id`, `LoaiBC`, `NgayCap`, `NgayHH`, `SCMND`, `TenBC`, `MaBC`, `NoiCap`) VALUES
(1, 'Bằn tốt nghiệp', '20/2/12008', '', 2323023, 'bằng tốt nghiệp câp 2', 1, 'sỡ giáo dục và dào tạo tỉnh Hà Giang');

-- --------------------------------------------------------

--
-- Structure de la table `nhanthan`
--

CREATE TABLE `nhanthan` (
  `chuc_vu` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dia_chi` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gioi_tinh` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `ho_ten_cha` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ho_ten_me` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL,
  `muc_luong` int(11) NOT NULL,
  `ngay_than_nam_sinh` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_vao` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `so_dien_thoai` int(11) NOT NULL,
  `ho_va_ten` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `nhanthan`
--

INSERT INTO `nhanthan` (`chuc_vu`, `dia_chi`, `gioi_tinh`, `ho_ten_cha`, `ho_ten_me`, `id`, `muc_luong`, `ngay_than_nam_sinh`, `ngay_vao`, `so_dien_thoai`, `ho_va_ten`) VALUES
('Tạp Vụ', 'Hà Đông-Hà Nọi', 'Nam', 'Lê Văn A', 'Lê Thị B', 1, 3000000, '20/2/1986', '2/12/2012', 1234567890, 'Lê Văn Luyện'),
('B?o V?', 'Hoàng Mai Hà N?i', 'Nam', '', '', 2, 300000, '21/11/1981', '20/12/2012', 1210212022, 'Kha Banh');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`MaAD`);

--
-- Index pour la table `banngcap`
--
ALTER TABLE `banngcap`
  ADD PRIMARY KEY (`MaBC`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `nhanthan`
--
ALTER TABLE `nhanthan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `MaAD` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `banngcap`
--
ALTER TABLE `banngcap`
  MODIFY `MaBC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `nhanthan`
--
ALTER TABLE `nhanthan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `banngcap`
--
ALTER TABLE `banngcap`
  ADD CONSTRAINT `bc` FOREIGN KEY (`id`) REFERENCES `nhanthan` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
